#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct{
	long* data;
	size_t size;
	int stepsize;
} buffer_t;

void buffer_init(buffer_t* buf, size_t size){
	buf->data = malloc(sizeof(long)*size);
	memset(buf->data, 0x00, size);
	buf->size = size;
	buf->stepsize = 20;
}

void buffer_add(buffer_t* buf, int index, long value){
	if (index >= buf->size -1) {
		buf->data = realloc(buf->data, (buf->size + buf->stepsize)*sizeof(long));
		buf->size += buf->stepsize;
	}
	buf->data[index] = value;
}

size_t readLine(char* buff, size_t buffsize){
	char* ptr = buff;
	size_t readBytes = 0;
	size_t totalBytes = 0;

	do{
		readBytes = read(0, ptr, 1);
		totalBytes += readBytes;
		ptr += 1;
	}while(readBytes != 0 && *(ptr -1) != '\n' && ptr < (buff+buffsize));
	 *(ptr -1) = '\0';
	return totalBytes;
}

void sort_bubble(long* arr, size_t arrsize){
	size_t i = 0;
	int swapped = 0;
	long temp = 0;

	do{
		swapped = 0;
		for(i = 1; i < arrsize; ++i){
			if (arr[i-1] > arr[i]) {
				temp = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = temp;
				swapped = 1;
			}
		}
	}while(swapped);
}

void print_arr(long* arr, size_t size){
	for(int i = 0; i < size; ++i){
		printf("%ld ", arr[i]);
	}
	printf("\n");
}

int main(){
	buffer_t buffer;
	buffer_init(&buffer, 10);

	int index = 0;
	size_t readBytes = 0;

	char cbuf[1024];

	do{
		readBytes = readLine(cbuf, sizeof(cbuf));
		if(readBytes > 0){
			buffer_add(&buffer, index, atol(cbuf));
			index += 1;
		}
	}while(readBytes > 0);
	//printf("all read\n");

	sort_bubble(buffer.data, index);

	print_arr(buffer.data, index);
}