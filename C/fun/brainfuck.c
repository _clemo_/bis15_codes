#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct stack_element{
	unsigned char* ptr;
	struct stack_element* last;
}stack_element_t;

typedef struct
{
	unsigned char* mem;
	size_t size;
	size_t used;
}memory_t;

void mem_init(memory_t* mem, size_t size){
	mem->mem = (unsigned char*) malloc(size);
	memset(mem->mem, 0x00, size);
	mem->size = size;
	mem->used = 0;
}

void mem_free(memory_t* mem){
	memset(mem->mem, 0x00, mem->size);
	free(mem->mem);
	mem->mem = NULL;
	mem->size = 0;
	mem->used = 0;
}

void mem_grow(memory_t* mem, size_t newsize){
	unsigned char* temp = (unsigned char*) malloc(newsize);
	memcpy(temp, mem->mem, mem->size);
	memset(temp + mem->size, 0x00, newsize - mem->size);
	unsigned char* old = mem->mem;
	mem->mem = temp;
	mem->size = newsize;
	free(old);
}

void mem_ensure_capacity(memory_t* mem, size_t pos){
	if (pos >= mem->size) {
		size_t new_size = pos + (pos % 1024);
		mem_grow(mem, new_size);
	}
}

void stack_init_item(stack_element_t* stack){
	stack->ptr = NULL;
	stack->last = NULL;
}

stack_element_t* stack_push(stack_element_t* stack, unsigned char* ptr){
	stack_element_t* item = (stack_element_t*) malloc(sizeof(stack_element_t));
	stack_init_item(item);
	item->ptr = ptr;
	item->last = stack;
	return item;
}

stack_element_t* stack_pop(stack_element_t* stack, unsigned char** ptr){
	*ptr = stack->ptr;
	stack_element_t* previous = stack->last;
	free(stack);
	return previous;
}

int brainfuck(unsigned char* input, size_t length){
	unsigned char* ptr = input;
	unsigned char* end = input + length;

	memory_t memory;
	mem_init(&memory, 100);
	size_t mem_current = 0;

	stack_element_t* call_stack = NULL;

	while(ptr != '\0' && ptr < end){
		switch(*ptr){
			case '+':
			case '-':
				if (*ptr == '+')
					*(memory.mem + mem_current) += 1;
				else
					*(memory.mem + mem_current) -= 1;
				ptr += 1;
				break;
			case '>':
				mem_current += 1;
				mem_ensure_capacity(&memory, mem_current);
				ptr += 1;
				break;
			case '<':
				if (mem_current == 0){
					printf("\nbuffer underflow\n");
					return 2;
				}
				mem_current -= 1;
				ptr += 1;
				break;
			case '[':
				if (*(memory.mem + mem_current) > 0) {
					call_stack = stack_push(call_stack, ptr);
					ptr += 1;
				}else{
					signed int stack_level = 0;
					while(stack_level >= 0){
						ptr += 1;
						switch(*ptr){
							case '[':
								stack_level += 1;
								break;
							case ']':
								stack_level -= 1;
								break;
						}
					}
					ptr += 1;
				}
				break;
			case ']':
				call_stack = stack_pop(call_stack, &ptr);
				break;
			case '.':
				printf("%c", *(memory.mem + mem_current));
				ptr += 1;
				break;
			case ',':
				*(memory.mem + mem_current) = getchar();
				ptr += 1;
				break;
			default:
				ptr += 1;
				break;
		}
	}
	mem_free(&memory);
	return 0;
}

int main(int argc, char const *argv[])
{
	if (argc != 2) {
		printf("give me parameter with file pointer!");
		return 1;
	}

	memory_t input_mem;
	mem_init(&input_mem, 1024);

	FILE *fp_input_file = NULL;
	fp_input_file = fopen(argv[1], "r");
	if (fp_input_file == NULL) {
		printf("Could not open the file\n");
		return 2;
	}

	size_t read = 0;
	do{
		mem_ensure_capacity(&input_mem, input_mem.used + 1024);
		read = fread(
		             input_mem.mem + input_mem.used,
		             sizeof(unsigned char),
		             1023,
		             fp_input_file
		            );
		input_mem.used += read;
	}while(read > 0);
	fclose(fp_input_file);

	int ret = brainfuck(input_mem.mem, input_mem.used);
	mem_free(&input_mem);
	return ret;
}