#include <stdio.h>
#include <stdint.h>

#define BOUNDARY 10000


/**
 * @brief      tests if value is prime
 * @param[in]  value  The value
 * @return     1 for prime else 0
 */
int primeTest(int value);


int main()
{
	int i;

	for(i = 2; i <= BOUNDARY; ++i){
		if ( primeTest(i) )
			printf("%d\n", i);
	}
	return 0;
}

int primeTest(int value){
	int i;
	int max = value / 2;
	if (value == 2) return 1;
	if (value % 2 == 0) return 0;

	for(i = 3; i <= max; i += 2){
		if (value % i == 0)
			return 0;
	}
	return 1;
}