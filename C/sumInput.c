#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

size_t readLine(char* buff, size_t buffsize){
	char* ptr = buff;
	size_t readBytes = 0;
	size_t totalBytes = 0;

	do{
		readBytes = read(0, ptr, 1);
		totalBytes += readBytes;
		ptr += 1;
	}while(readBytes != 0 && *(ptr -1) != '\n' && ptr < (buff+buffsize));
	*(ptr -1)= '\0';
	return totalBytes;
}

int main()
{
	char* buffer     = malloc(1024);
	size_t buffsize  = 1024;
	size_t readBytes = 0;
	long long summe  = 0;

	do{
		readBytes = readLine(buffer, buffsize);
		if(readBytes > 0){
			summe += atoll(buffer);
		}
	}while(readBytes != 0);
	printf("summe: %lld \n", summe);
	free(buffer);
	return 0;
}
