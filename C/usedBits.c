#include <stdio.h>
#include <math.h>
#include <stdint.h>

unsigned int bitsUsed2(int32_t value){
	for(int i = 31; i >= 0; --i)
		if(value & (1<<i)){
			return i;
		}
	return 0;
}

unsigned int bitsUsed(int value){
	double info = log2((double) value);
	return (int) ceil(info);
}

int main(){
	for(int i = 0; i < 100; ++i)
		printf("%d: %d bits\n", i ,bitsUsed2(i));
}