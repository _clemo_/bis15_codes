public class Book {
  String title;
  boolean borrowed;
  // Creates a new Book
  public Book(String bookTitle) {
    this.title = bookTitle;
  }
  // Marks the book as rented
  public void borrowed() throws Exception {
    if(borrowed)
    	throw(new Exception("Book is already borrowed!"));
    else
    	borrowed = true;
  }
  // Marks the book as not rented
  public void returned() throws Exception {
	  if(!borrowed)
	    	throw(new Exception("Book is not borrowed!"));
	    else
	    	borrowed = true;
  }
  // Returns true if the book is rented, false otherwise
  public boolean isBorrowed() {
	  return borrowed;
  }
  
  // Returns the title of the book
  public String getTitle() {
    return title;
  }
  public static void main(String[] args) throws Exception {
    // Small test of the Book class
    Book example = new Book("The Da Vinci Code");
    System.out.println("Title (should be The Da Vinci Code): " + example.getTitle());
    System.out.println("Borrowed? (should be false): " + example.isBorrowed());
    example.borrowed();
    System.out.println("Borrowed? (should be true): " + example.isBorrowed());
    example.returned();
    System.out.println("Borrowed? (should be false): " + example.isBorrowed());
  }
}