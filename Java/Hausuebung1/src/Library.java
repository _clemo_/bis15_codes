import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

public class Library {
	String location;
	Map<String, Book> books;

	public Library(String location) {
		this.location = location;
		books = new HashMap<String, Book>();
	}

	// Add missing implementation to this class
	public static void main(String[] args) throws Exception {
		// Create two libraries
		Library firstLibrary = new Library("10 Main St.");
		Library secondLibrary = new Library("228 Liberty St.");
		// Add four books to the first library
		firstLibrary.addBook(new Book("The Da Vinci Code"));
		firstLibrary.addBook(new Book("Le Petit Prince"));
		firstLibrary.addBook(new Book("A Tale of Two Cities"));
		firstLibrary.addBook(new Book("The Lord of the Rings"));
		// Print opening hours and the addresses
		System.out.println("Library hours:");
		printOpeningHours();
		System.out.println();
		System.out.println("Library addresses:");
		firstLibrary.printAddress();
		secondLibrary.printAddress();
		System.out.println();
		// Try to borrow The Lord of the Rings from both libraries
		System.out.println("Borrowing The Lord of the Rings:");
		firstLibrary.borrowBook("The Lord of the Rings");
		firstLibrary.borrowBook("The Lord of the Rings");
		secondLibrary.borrowBook("The Lord of the Rings");
		System.out.println();
		// Print the title of all books available from both libraries
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
		System.out.println();
		System.out.println("Books available in the second library:");
		secondLibrary.printAvailableBooks();
		System.out.println();
		// Return The Lord of the Rings to the first library
		System.out.println("Returning The Lord of the Rings");
		firstLibrary.returnBook("The Lord of the Rings");
		System.out.println();
		// Print the titles of all books available from the first library
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
	}

	private void returnBook(String string) {
		if (books.containsKey(string)) {
			try {
				books.get(string).returned();
				System.out.printf("You successfully returned %s.\n", string);
			} catch (Exception e) {
				System.out.println("Sorry, this book is already back");
			}
		} else {
			System.out.println("Sorry, this book is not in our borrowed");
		}
	}


	private void printAvailableBooks() {
		if(books.size() == 0)
			System.out.println("No books in catalog");
		else{
			for (String title : books.keySet()) {
				System.out.println(title);
			}
		}
	}

	private void borrowBook(String string) {
		if (books.containsKey(string)) {
			try {
				books.get(string).borrowed();
				System.out.printf("You successfully borrowed %s.\n", string);
			} catch (Exception e) {
				System.out.println("Sorry, this book is already borrowed");
			}
		} else {
			System.out.println("Sorry, this book is not in our catalog");
		}
	}

	private void printAddress() {
		System.out.println(this.location);
	}

	private static void printOpeningHours() {
		System.out.println("Libraries are open daily from 9am to 5pm.");
	}

	private void addBook(Book book) throws Exception {
		if (this.books.containsKey(book.getTitle()))
			throw (new Exception("Book is already in the library"));
		books.put(book.getTitle(), book);
	}
}