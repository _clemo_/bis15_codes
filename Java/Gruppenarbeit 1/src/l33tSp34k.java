import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

public class l33tSp34k {
	public static void getLeetSpeak() throws IOException {
		// read mode
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Wähle eine Option:");
		System.out.println("   1. normal  >   1337");
		System.out.println("   2.   1337  > normal");
		System.out.print("Geben Sie eine Nummer ein: ");
		int modus =  Integer.parseInt(br.readLine());
		
		// read input
		System.out.print("Geben Sie einen Text ein:");
		
		String text = br.readLine();

		// input to lower and write to array
		char[] result = text.toLowerCase().toCharArray();

		// create translation table
		char[] from = { 'a', 'e', 'o', 's', 't', 'b', 'i' };
		char[] to =   { '@', '3', '0', '$', '7', '8', '1' };

		// decide direction for translation
		char[] use_to, use_from;
		if (modus == 1) {
			use_to = to;
			use_from = from;
		} else {
			use_to = from;
			use_from = to;
		}

		Random r = new Random();

		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < use_from.length; j++) {
				if (result[i] == use_from[j])
					result[i] = use_to[j];
			}

			if (r.nextBoolean())
				result[i] = ("" + result[i]).toUpperCase().charAt(0);
		}

		System.out.println(new String(result));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
