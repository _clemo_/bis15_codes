
public class TimeBudget {
	public static double getSalery(double payment, int workingHours) throws Exception{
		if(payment < 8)
			throw(new Exception("Ihre Mitarbeiter sollten mehr Lohn bekommen!"));
		if(workingHours > 60)
			throw(new Exception("Ihre Mitarbeiter benötigen Ruhe!"));
		
		double salery = 0;
		if(workingHours > 41){
			salery +=  (workingHours-41)*payment*1.5;
			workingHours = 41;
		}
		salery += payment*workingHours;
		
		return salery;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
