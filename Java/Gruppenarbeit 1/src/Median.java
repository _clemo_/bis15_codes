import java.util.Scanner;
import java.util.Arrays;

public class Median {
	public static void getMedian(){
		System.out.print("Wie viele Zahlen werden eingegeben?");
		Scanner in = new Scanner(System.in);
		int anzahl = in.nextInt();
		
		int[] input = new int[anzahl];
		for (int i = 0; i < anzahl; i++) {
			System.out.printf("Geben Sie Zahl %d ein: ", i+1);
			input[i] = in.nextInt();
		}
		
		Arrays.sort(input);
		
		if(anzahl % 2 == 1)
			System.out.println(input[anzahl/2]);
		else
			System.out.println((input[anzahl/2-1]+input[anzahl/2])/2);
	}
	
	public static void main(String[] args){}
}


