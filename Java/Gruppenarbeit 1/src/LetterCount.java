import java.util.Scanner;


public class LetterCount {
	public static void getLetterCount(){
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		
		System.out.print("Welcher Buchstabe soll gezählt werden?");
		char c = in.nextLine().toCharArray()[0];
				
		System.out.print("In welcher Zeichenkette? ");
		String s = in.nextLine();
		
		int cnt = 0;

		for(char x : s.toCharArray())
			if(x == c)
				++cnt;
		
		System.out.println(cnt);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
