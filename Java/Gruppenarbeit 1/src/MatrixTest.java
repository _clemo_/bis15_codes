import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Random;

import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class MatrixTest {
	String[] inputs = { "5\r\n5\r\n","3\r\n6\r\n" };
	String[] outputs = { "[500, 250, 1, 750, 1000]\n[500, 250, 1, 750, 1000]\n[500, 250, 1, 750, 1000]\n[500, 250, 1, 750, 1000]\n[500, 250, 1, 750, 1000]\n" ,"[500, 250, 1, 750, 1000, 500]\n[1, 750, 1000, 500, 250, 1]\n"};

	@Test
	public void testRandomInject() throws NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = Math.class.getDeclaredField("randomNumberGenerator");
		field.setAccessible(true);
		field.set(null, new Random() {
			double[] vals = { 1, 0.5, 0.25, 0, 0.75 };
			int pos = 0;

			@Override
			public double nextDouble() {
				return vals[(pos = ++pos % vals.length)];
			}

		});

		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		try {
			Matrix.testRnd(5);
		} finally {
			System.setOut(null);
		}
		assertTrue(outContent.toString().equals("0.5\n0.25\n0.0\n0.75\n1.0\n"));

		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		try {
			Matrix.testRnd(10);
		} finally {
			System.setOut(null);
		}

		assertTrue(outContent.toString().equals(
				"0.5\n0.25\n0.0\n0.75\n1.0\n0.5\n0.25\n0.0\n0.75\n1.0\n"));

		field.set(null, new Random());
	}

	@Test
	public void testRandom5_5() throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {
		Field field = Math.class.getDeclaredField("randomNumberGenerator");
		field.setAccessible(true);
		field.set(null, new Random() {
			double[] vals = { 1, 0.5, 0.25, 0, 0.75 };
			int pos = 0;

			@Override
			public double nextDouble() {
				return vals[(pos = ++pos % vals.length)];
			}

		});

		for (int i = 0; i < inputs.length; i++) {
			String data = inputs[i];

			ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));

			InputStream stdin = System.in;

			try {
				System.setIn(new ByteArrayInputStream(data.getBytes()));
				Matrix.getMatrix();
			} finally {
				System.setIn(stdin);
				System.setOut(null);
			}
			String s = outContent.toString();

			assertTrue(s.endsWith(outputs[i]));
		}
		field.set(null, new Random());
	}

}
