import static org.junit.Assert.*;

import org.junit.Test;

public class PrimeTest {
	int[] inputs = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 100000, 100193, 1000231,
			100000267, 100140049 };
	boolean[] result = { true, true, false, true, false, true, false, false,
			false, false, true, true, true, false };

	@Test
	public void test_ugly() { //32.807 s
		for (int x = 0; x < 100; x++)
			for (int i = 0; i < inputs.length; i++) {
				assertTrue(Prime.isPrime_ugly(inputs[i]) == result[i]);
			}
	}

	@Test
	public void test_better() { // 16.379 s
		for (int x = 0; x < 100; x++)
			for (int i = 0; i < inputs.length; i++) {
				//System.out.printf("%d", inputs[i]);
				assertTrue(Prime.isPrime_better(inputs[i]) == result[i]);
				//System.out.println(" -> " + result[i]);
			}
	}

	@Test
	public void test_much_better() { // 0.013 s
		for (int x = 0; x < 100; x++)
			for (int i = 0; i < inputs.length; i++) {
				assertTrue(Prime.isPrime_much_better(inputs[i]) == result[i]);
			}
	}

	@Test
	public void test_best() { // 0.006 s
		for (int x = 0; x < 100; x++)
			for (int i = 0; i < inputs.length; i++) {
				assertTrue(Prime.isPrime_best(inputs[i]) == result[i]);
			}

	}

	@Test
	public void test_xoh() { // 0.005 s
		for (int x = 0; x < 100; x++)
			for (int i = 0; i < inputs.length; i++){
				//System.out.println(inputs[i] + " -> " + result[i]);
				assertTrue(Prime.isPrime_xoh(inputs[i]) == result[i]);
			}
	}
}
