import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.Test;

public class l33tSp34kTest {
	String[] inputs = {"1\r\nHallo\r\n","2\r\nh@LL0\r\n","1\r\nich bin so elite!\r\n","2\r\n1337"};
	String[] outputs = {"h@ll0", "hallo","1ch 81n $0 3l173!","ieet"};

	@Test
	public void testTranslation() throws IOException {
		for (int i = 0; i < inputs.length; ++i) {
			String data = inputs[i];
			InputStream stdin = System.in;
			ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));

			try {
				System.setIn(new ByteArrayInputStream(data.getBytes()));

				
				l33tSp34k.getLeetSpeak();
				
				boolean res = (outContent.toString().toLowerCase().endsWith(outputs[i])
						|| outContent.toString().toLowerCase().endsWith(outputs[i] + "\n")
						|| outContent.toString().toLowerCase().endsWith(outputs[i] + "\r\n"));
				assertTrue(res);
			} finally {
				System.setIn(stdin);
				System.setOut(null);
			}
		}
	}

}
