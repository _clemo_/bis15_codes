import java.util.Arrays;
import java.util.Scanner;

public class Matrix {

	public static void testRnd(int n) {
		for (int i = 0; i < n; ++i)
			System.out.println(Math.random());
	}

	public static void getMatrix() {
		Scanner in = new Scanner(System.in);

		// ask user
		System.out.print("Geben Sie n ein: ");
		int n = in.nextInt();
		System.out.print("Geben Sie x ein: ");
		int x = in.nextInt();

		// alloc and fill matrix
		int[][] matrix = new int[n][];

		for (int i = 0; i < n; ++i) {
			matrix[i] = new int[x];
			for (int j = 0; j < x; ++j)
				matrix[i][j] = (int) (Math.random() * 999 + 1);
		}

		// generate sum
		int[] sum = new int[n];
		for (int i = 0; i < sum.length; i++) {
			int tmp = 0;
			for (int j = 0; j < x; j++) {
				tmp += matrix[i][j];
			}
			sum[i] = tmp;
		}

		// find largest/smallest sum:
		int largestSum = -1;
		int smallestSum = -1;
		for (int i = 0; i < sum.length; i++) {
			if (largestSum == -1 || largestSum < sum[i])
				largestSum = sum[i];
			if (smallestSum == -1 || smallestSum > sum[i])
				smallestSum = sum[i];
		}

		// print smallest sum
		for (int i = 0; i < sum.length; i++) {
			if(sum[i] == largestSum)
				System.out.println(Arrays.toString(matrix[i]));
		}
		
		//print smallest sum
		for (int i = 0; i < sum.length; i++) {
			if(sum[i] == smallestSum)
				System.out.println(Arrays.toString(matrix[i]));
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
