import static org.junit.Assert.*;

import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

public class LetterCountTest {
	String[] inputs = {"1\r\n15", "a\r\nabcdefg", "*\r\n*.*.***.***#!adasdsad*"};
	String[] outputs = {"1","1", "9"};
	
	@Test
	public void test() {
		for(int i = 0; i < inputs.length; ++i){
			String data = inputs[i];

			InputStream stdin = System.in;
	 		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));


			try {
				System.setIn(new ByteArrayInputStream(data.getBytes()));

				LetterCount.getLetterCount();
				
				assertTrue(outContent.toString().endsWith(outputs[i]) || outContent.toString().endsWith(outputs[i]+"\n")  || outContent.toString().endsWith(outputs[i]+"\r\n"));
			} finally {
				System.setIn(stdin);
				System.setOut(null);
			}
		}
	}

}
