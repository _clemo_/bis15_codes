import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.Test;

public class MedianTest {
	String[] inputs = {"1\r\n15", "5\r\n5\r\n100\r\n2\r\n10\r\n7", "6\r\n5\r\n100\r\n2\r\n10\r\n7\r\n7"};
	String[] outputs = {"15","7", "7"};


	@Test
	public void test() {
		for(int i = 0; i < inputs.length; ++i){
			String data = inputs[i];

			InputStream stdin = System.in;
	 		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));


			try {
				System.setIn(new ByteArrayInputStream(data.getBytes()));

				Median.getMedian();

				assertTrue(outContent.toString().endsWith(outputs[i]) || outContent.toString().endsWith(outputs[i]+"\n")  || outContent.toString().endsWith(outputs[i]+"\r\n"));
			} finally {
				System.setIn(stdin);
				System.setOut(null);
			}
		}
	}

}
