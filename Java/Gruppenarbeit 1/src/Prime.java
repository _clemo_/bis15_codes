import java.util.Scanner;

public class Prime {

	public static boolean isPrime_ugly(int n) {
		for (int i = 2; i < n; ++i)
			if (n % i == 0)
				return false;
		return true;
	}

	public static boolean isPrime_better(int n) {
		if (n < 2)
			return false;
		if (n < 4)
			return true;
		for (int i = 2; i < (n / 2)+1; ++i)
			if (n % i == 0)
				return false;
		return true;
	}

	public static boolean isPrime_much_better(int n) {
		for (int i = 2, max = (int) (Math.sqrt(n) + 1); i < max; ++i)
			if (n % i == 0)
				return false;
		return true;
	}

	public static boolean isPrime_best(int n) {
		if (n == 2)
			return true;
		if (n % 2 == 0)
			return false;
		for (int i = 3, max = (int) (Math.sqrt(n) + 1); i < max; i += 2)
			if (n % i == 0)
				return false;
		return true;
	}

	public static boolean isPrime_xoh(int n) {
		if (n < 2)
			return false;
		if (n < 4)
			return true;
		if (n % 2 == 0 || n % 3 == 0)
			return false;
		for (int i = 6, max = (int)(Math.sqrt(n) + 1); i <= max; i += 6) {
			if (n % (i - 1) == 0 || n % (i + 1) == 0)
				return false;
		}
		return true;
	}
	
	public static void getPrimesBetween(){
		Scanner in = new Scanner(System.in);
		System.out.print("Geben Sie die Untergrenze ein: ");
		int untergrenze = in.nextInt();
		System.out.print("Geben Sie die Obergrenze ein: ");
		int obergrenze = in.nextInt();
		
		for(int i = untergrenze; i < obergrenze; ++i)
			if(isPrime_xoh(i))
				System.out.print(i+", ");
	}
}
