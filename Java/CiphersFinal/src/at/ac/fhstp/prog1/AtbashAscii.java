package at.ac.fhstp.prog1;

public class AtbashAscii extends AtbashLatin {
	public AtbashAscii() {
		firstLetter = ' ';
		lastLetter = '~';
		makeLowercase = false;
	}
	
	public static void main(String[] args) {
		Cipher atbashAscii = new AtbashAscii();
		System.out.println("Should be: Delete all Keys and burn the $$$.");
		System.out.println(atbashAscii.decrypt(
				"Z929*9~=22~S9%+~=0:~<),0~*69~zzzp"));
	}
}
