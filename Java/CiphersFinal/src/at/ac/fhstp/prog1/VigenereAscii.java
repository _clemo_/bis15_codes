package at.ac.fhstp.prog1;

import java.util.Arrays;

public class VigenereAscii extends VigenereLatin{

	public VigenereAscii(String key) {
		super(key);
		alpha =  Arrays.asList(new Character[]{
				' ','!','\"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}','~'
		});
		
		stripWS = false;
	}
	
	public static void main(String[] args) {
		String shortVigenereKey = "v1genere";
		Cipher vigenereAscii = new VigenereAscii(shortVigenereKey);
		System.out.println(vigenereAscii.decrypt(
		"Nyae]Ugel%MePeeKX}gIXV[Ki1WXnGrRf OKae^KpP"));
		System.out.println(vigenereAscii.getStrengthRating());
	}

}
