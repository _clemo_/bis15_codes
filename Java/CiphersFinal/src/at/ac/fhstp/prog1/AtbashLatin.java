package at.ac.fhstp.prog1;

public class AtbashLatin extends Cipher {
	protected char firstLetter;
	protected char lastLetter;
	protected boolean makeLowercase;
	
	public AtbashLatin() {
		firstLetter = 'a';
		lastLetter  = 'z';
		makeLowercase = true;
	}
	
	@Override
	public String getStrengthRating() {
		return RESISTANCE[1];
	}

	@Override
	public String encrypt(String plaintext) {
		if(makeLowercase)
			plaintext = plaintext.toLowerCase();
		char[] letters = plaintext.replaceAll("\\s", "").toCharArray();
		
		for (int i = 0; i < letters.length; i++)
			if(letters[i] < firstLetter || letters[i] > lastLetter){
				System.err.println("Invalid character found in plaintext!");
				return "";
			}
			else
				letters[i] = (char) (lastLetter-(letters[i] - firstLetter));
		return new String(letters);
	}

	@Override
	public String decrypt(String ciphertext) {
		return encrypt(ciphertext);
	}
	
	public static void main(String[] args) {
		Cipher atbashLatin = new AtbashLatin();
		
		System.out.println("1. sould be: nvvgnvzgnrwmrtsg");
		System.out.println(atbashLatin.encrypt("Meet me at Midnight"));
		System.out.println("2. shoud be: Invalid character found in plaintext!");
		System.out.println(atbashLatin.encrypt("Meet me @Midnight."));
	}
}
