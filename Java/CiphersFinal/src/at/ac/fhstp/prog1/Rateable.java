package at.ac.fhstp.prog1;

public interface Rateable {
	public final String[] RESISTANCE = { "n0ne", "kid sister", "big sister",
			"100 MHz CPU", "major governments" };

	public String getStrengthRating();
}