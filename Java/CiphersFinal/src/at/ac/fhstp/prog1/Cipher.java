package at.ac.fhstp.prog1;

public abstract class Cipher implements Rateable {
	public abstract String encrypt(String plaintext);

	public abstract String decrypt(String ciphertext);
}