package at.ac.fhstp.prog1;

import java.util.Arrays;
import java.util.List;

public class VigenereLatin extends Cipher{
	protected List<Character> alpha = Arrays.asList(new Character[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}','~',' ','!','\"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_','`'});
	// nein mir war ned fad, ich hab das array in da interaktiven ruby shell generiert....
	protected boolean stripWS = true;
	
	private String key;
	private int lastLen = 0;
	
	public VigenereLatin(String key) {
		this.key = key;
	}
	
	public char encryptChar(char plain, char key){
		int numPlain = alpha.indexOf(plain);
		int numKey = alpha.indexOf(key);
		int numNew = (numPlain+numKey)%alpha.size();
		return alpha.get(numNew);
	}
	
	public char decryptChar(char cipher, char key){
		int numCipher = alpha.indexOf(cipher);
		int numKey = alpha.indexOf(key);
		
		int numNew = (numCipher-numKey)%alpha.size();
		while (numNew < 0) {numNew += alpha.size();}
		
		return alpha.get(numNew);
	}
	
	public char[] expandKey(int len){
		char[] res = new char[len];
		for (int i = 0; i < len; i++) {
			res[i] = key.charAt(i%key.length());
		}
		return res;
	}
	
	@Override
	public String getStrengthRating() {
		return RESISTANCE[key.length() < lastLen ? 2 : 4];
	}

	@Override
	public String encrypt(String plaintext) {
		if(stripWS)
			plaintext = plaintext.toLowerCase().replaceAll("\\s", "");
		this.lastLen = plaintext.length();
		char[] c = plaintext.toCharArray();
		char[] k = expandKey(plaintext.length());
		for (int i = 0; i < c.length; i++) {
			c[i] = encryptChar(c[i], k[i]);
		}
		return new String(c);
	}

	@Override
	public String decrypt(String ciphertext) {
		this.lastLen = ciphertext.length();
		char[] c = ciphertext.toCharArray();
		char[] k = expandKey(ciphertext.length());
		for (int i = 0; i < c.length; i++) {
			c[i] = decryptChar(c[i], k[i]);
		}
		return new String(c);
	}

	public static void main(String[] args) {
		String longVigenereKey =
				"koancadlocevAcsEbOcOceejbennIrropduxUlArjEykEebrOgdarovOshej";
		Cipher vigenereLatin = new VigenereLatin(longVigenereKey);
		System.out.println(vigenereLatin.encrypt(
		"OTPs are secure but the key exchange is tricky"));
		System.out.println(vigenereLatin.decrypt("y#p!crh~sey(Ed(XuVgYg}i\"dln{Ovz\"$u}z_%"));
		System.out.println(vigenereLatin.getStrengthRating());
	}
}
