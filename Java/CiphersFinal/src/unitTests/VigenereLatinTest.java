 package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import at.ac.fhstp.prog1.VigenereLatin;

public class VigenereLatinTest {
	@Test
	public void testROT0() {
		try{
			VigenereLatin cipher = new VigenereLatin("a");
			String r = cipher.encrypt("uiuaa");
			assertTrue("if key is equals to the lower-border, there is no crypto applied!",r.equals("uiuaa"));
			r = cipher.decrypt(r);
			assertTrue("if key is 'a', there is no crypto applied!",r.equals("uiuaa"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}

	@Test
	public void testWikipedia() {
		try{
			VigenereLatin cipher = new VigenereLatin("lemon");
			String r = cipher.encrypt("ATTACKATDAWN");
			assertTrue("expected :'lx!opve!rn#r'",r.equals("lx!opve!rn#r"));
			r = cipher.decrypt(r);
			assertTrue("expected :'attackatdawn'",r.equals("attackatdawn"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testSpace(){
		try{
			VigenereLatin cipher = new VigenereLatin("a");
			assertTrue("expected :'aa' since qhite spaces are stripped",cipher.encrypt("a a").equals("aa"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testMultiSpace(){
		try{
			VigenereLatin cipher = new VigenereLatin("a");
			assertTrue("expected :'aa' since white spaces are stripped",cipher.encrypt("a   a").equals("aa"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testTab(){
		try{
			VigenereLatin cipher = new VigenereLatin("a");
			assertTrue("expected :'aa' since white spaces(hint: use \\s) are stripped",cipher.encrypt("a\ta").equals("aa"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testEncryptAngabeLong() {
		try{
			VigenereLatin cipher = new VigenereLatin("koancadlocevAcsEbOcOceejbennIrropduxUlArjEykEebrOgdarovOshe");
			
			String r =cipher.encrypt("OTPs are secure but the key exchange is tricky");
			assertTrue(r.equals("y#p!crh~sey(Ed(XuVgYg}i\"dln{Ovz\"$u}z_%"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
}
