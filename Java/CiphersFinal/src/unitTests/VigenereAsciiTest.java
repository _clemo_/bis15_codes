package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import at.ac.fhstp.prog1.Cipher;
import at.ac.fhstp.prog1.VigenereAscii;
import at.ac.fhstp.prog1.VigenereLatin;

public class VigenereAsciiTest {
	@Test
	public void testROT0() {
		try{
			VigenereAscii cipher = new VigenereAscii(" ");
			String r = cipher.encrypt("uiuaa");
			assertTrue("if key is equals to the lower-border, there is no crypto applied!",r.equals("uiuaa"));
			r = cipher.decrypt(r);
			assertTrue("if key is ' ', there is no crypto applied!",r.equals("uiuaa"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testSpace(){
		try{
			VigenereAscii cipher = new VigenereAscii(" ");
			assertTrue("expected :'a a' since white spaces are NOT stripped",cipher.encrypt("a a").equals("a a"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testUppercase(){
		try{
			VigenereAscii cipher = new VigenereAscii(" ");
			assertTrue("expected :'A A' since lowercase should not be used",cipher.encrypt("A A").equals("A A"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testDecryptAngabeShort() {
		try{
			Cipher cipher = new VigenereAscii("v1genere");
			String s = cipher.decrypt(
			"Nyae]Ugel%MePeeKX}gIXV[Ki1WXnGrRf OKae^KpP");

			if(s.equals("9J[aPQVaWUGaCaTGCNaEKRJGTaQTaCaNQPIGTaMG[!"))                        //BITTE KLARER IN DER ANGABE FORMULIEREN!!!
				assertTrue("sorry bei ASCII ist das erste Zeichen ' ' und nicht 'a'",false);
			assertTrue(s.equals("Why not use a real cipher or a longer key?"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testEncryptAngabeShort() {
		try{
			Cipher cipher = new VigenereAscii("v1genere");
			String s = cipher.encrypt("Why not use a real cipher or a longer key?");
			if(s.length() +8 == "Why not use a real cipher or a longer key?".length() ){
				assertTrue("Spaces should not be stripped in ASCII", false);
			}
			
			assertTrue(s.equals("Nyae]Ugel%MePeeKX}gIXV[Ki1WXnGrRf OKae^KpP"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}

}
