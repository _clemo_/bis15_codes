package unitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import at.ac.fhstp.prog1.AtbashLatin;

public class AtbashTest {

	@Test
	public void testIllegalInput() {
		AtbashLatin ab = new AtbashLatin();
		
		try{
			String result = ab.encrypt("#!!!#");
			if(result == null || result.equals("") || result.equals("#!!!#")){
				assertTrue(true);
			}else{
				assertTrue("encrypt with illegal arguments should not return something useful...", false);
			}
		}catch(Exception e){
			assertTrue(true);
		}
	}
	
	@Test
	public void testSingleCharacters(){  // simpliest possible test
		AtbashLatin ab = new AtbashLatin();
		try{
			assertTrue("expected 'z'",ab.encrypt("a").equals("z"));
			assertTrue("expected 'y'",ab.encrypt("b").equals("y"));
			assertTrue("expected 'x'",ab.encrypt("c").equals("x"));
			assertTrue("expected 'w'",ab.encrypt("d").equals("w"));
			assertTrue("expected 'v'",ab.encrypt("e").equals("v"));
			assertTrue("expected 'u'",ab.encrypt("f").equals("u"));
			assertTrue("expected 't'",ab.encrypt("g").equals("t"));
			assertTrue("expected 's'",ab.encrypt("h").equals("s"));
			assertTrue("expected 'r'",ab.encrypt("i").equals("r"));
			assertTrue("expected 'q'",ab.encrypt("j").equals("q"));
			assertTrue("expected 'p'",ab.encrypt("k").equals("p"));
			assertTrue("expected 'o'",ab.encrypt("l").equals("o"));
			assertTrue("expected 'n'",ab.encrypt("m").equals("n"));
			assertTrue("expected 'm'",ab.encrypt("n").equals("m"));
			assertTrue("expected 'l'",ab.encrypt("o").equals("l"));
			assertTrue("expected 'k'",ab.encrypt("p").equals("k"));
			assertTrue("expected 'j'",ab.encrypt("q").equals("j"));
			assertTrue("expected 'i'",ab.encrypt("r").equals("i"));
			assertTrue("expected 'h'",ab.encrypt("s").equals("h"));
			assertTrue("expected 'g'",ab.encrypt("t").equals("g"));
			assertTrue("expected 'f'",ab.encrypt("u").equals("f"));
			assertTrue("expected 'e'",ab.encrypt("v").equals("e"));
			assertTrue("expected 'd'",ab.encrypt("w").equals("d"));
			assertTrue("expected 'c'",ab.encrypt("x").equals("c"));
			assertTrue("expected 'b'",ab.encrypt("y").equals("b"));
			assertTrue("expected 'a'",ab.encrypt("z").equals("a"));
		}catch(Exception e){
			assertTrue("should not crash here...",false);
		}
	}
	
	@Test
	public void someUppercaseInput() {
		AtbashLatin ab = new AtbashLatin();
		
		try{
			String result = ab.encrypt("ABC");
			assertTrue("Expected 'zyx'", result.equals("zyx"));
		}catch(Exception e){
			assertTrue("should not crash here",false);
		}
	}
	
	@Test
	public void testWhitespace() {
		AtbashLatin ab = new AtbashLatin();
		
		try{
			String result = ab.encrypt("A A");
			assertTrue("Expected 'zz' since whitespaces should be stripped", result.equals("zz"));
		}catch(Exception e){
			assertTrue("should not crash here",false);
		}
	}
	
	@Test
	public void testMultipleWhitespace() {
		AtbashLatin ab = new AtbashLatin();
		
		try{
			String result = ab.encrypt("A   A");
			assertTrue("Expected 'zz' since whitespaces should be stripped", result.equals("zz"));
		}catch(Exception e){
			assertTrue("should not crash here",false);
		}
	}
	
	@Test
	public void testTab(){
		AtbashLatin ab = new AtbashLatin();
		
		try{
			assertTrue("expected :'zz' since white spaces(hint: use \\s) are stripped",ab.encrypt("a\ta").equals("zz"));
		}catch(Exception e){
			assertTrue("the program should not crash here",false);
		}
	}
	
	@Test
	public void testAusDerAngabe() {
		AtbashLatin ab = new AtbashLatin();
		
		try{
			String result = ab.encrypt("Meet me at Midnight");
			assertTrue("Expected 'nvvgnvzgnrwmrtsg'", result.equals("nvvgnvzgnrwmrtsg"));
			
			result = ab.decrypt(result);
			assertTrue("Expected 'meetmeatmidnight'", result.equals("meetmeatmidnight"));
		}catch(Exception e){
			assertTrue("should not crash here",false);
		}
	}
}
