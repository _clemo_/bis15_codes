package inGruppe;

import at.ac.fhstp.prog1.Cipher;

//erlebniserzählung zu Vigenere als LATIN
public class VigLatinErlebnis extends Cipher {
	/******************* Nur für LATIN Instanz *********************/
	private String key;                                            // Merkt sich den Schlüssel 
	private int inputLength = 0;                                   // Merkt sich die zuletzt benutze Länge
	/***************************************************************/
	
	/****************** Nur für LATIN ******************************/
	private static int anzahlBuchstaben = 95;                      // Merkt sich die Anzahl der Buchstaben im Alphabet
	private static char erstesZeichen = ' ';                       // Merkt sich das erste Zeichen des Alphabet
	/***************************************************************/
	
	/****************** Für LATIN und Ableitungen ******************/
	protected int offset = 'a'-erstesZeichen;                      // Merkt sich den Wert der benötigt wird, dass 'a' der erste Buchstabe ist
	/***************************************************************/
	
	/****************** Konstruktor ********************************/
	public VigLatinErlebnis(String k) {                            // Da wir einen Schlüssel benötigen
		key = k;                                                   // müssen wir diesen auch speichern
	}                                                              // damit wir ihn später nutzen können
	/***************************************************************/
	
	/******************** LATIN ('a' = begin) **********************/
	public String prepareText(String input){                       // vor dem Verschlüsseln wird muss der Text verändert werden
		return input.toLowerCase()                                 // indem er zu Kleinbuchstaben gemacht wird und 
				.replaceAll("\\s", "");                            // alle Whitespaces entfernt werden
	}                                                              // diese Funktion kann dann beim Verschlüsseln benutzt werden
	public String encrypt(String plaintext){                       // zum Verschlüsseln
		inputLength = plaintext.length();                          //#merke Eingabe-Länge für spätere Stärken-Berechnung
		char[] key = new char[plaintext.length()];                 // lege char[] in größe von input an
		for (int i = 0; i < key.length; i++) {                     // und fülle das array 
			key[i] = this.key.charAt(i % this.key.length());       // mit dem key auf (wiederhole key wenn nötig)
		}                                                          // danch
		char[] text = prepareText(plaintext).toCharArray();        // wird der Input vorbereitet und zu einem char[] gemacht
		for (int i = 0; i < text.length; i++) {                    // anschließend gehe den text durch
			if(text[i] < 'a' || text[i] > 'z'){                    // prüfe ob input zeichen ungültig
				System.err.println("invalid input");               // wenn ungültig beschimpfe user
				return null;                                       // und gibt NIX zurück!
			}                                                      // ansonsten
			int numInAlphaText = text[i] - erstesZeichen;          // ermittle die nummer des buchstaben in unserem alphabet (' ' bis '~')
			int numInAlphaKey  =  key[i] - erstesZeichen;          // und die nummer für den key
			int posNeusZeichen = numInAlphaKey+numInAlphaText;     // ermittle die neue position für das zeichen indem key+input gerechnet wird
			posNeusZeichen -= offset;                              // und "verschiebe" das neue Zeichen um einen Wert damit s 'a' das erste Zeichen ist
			while(posNeusZeichen < 0)                              // wenn die Zahl jetzt aber unter 0 läuft
				posNeusZeichen += anzahlBuchstaben;                // addiere Anzahl der Buchstaben hinzu (muss so gemacht werden, da % nicht < 0 funtkioniert)
			posNeusZeichen = posNeusZeichen % anzahlBuchstaben;    // und stelle sicher dass der Wert innerhalb des Alphabetes bleibt
			posNeusZeichen += erstesZeichen;                       // danach erzeuge druckbares Zeichen indem wieder vom ersten Zeichen ausgegangen wird
			text[i] = (char) posNeusZeichen;                       // zuletzt wird die Zahl in einen Buchstaben verwandelt und erstetzt das Eingabe-Zeichen
		}                                                          // nachdem alle Zeichen ersetzt wurden
		return new String(text);                                   // gib neuen String aus den ersetzten Zeichen zurück
	}                                                              // und bin fertig mit dem Verschlüsseln
	public String decrypt(String ciphertext){                      // damit wir enschlüsseln können
		inputLength = ciphertext.length();                         //#merke Eingabe-Länge für spätere Stärken-Berechnung
		char[] key = new char[ciphertext.length()];                // lege char[] in größe von input an
		for (int i = 0; i < key.length; i++) {                     // und fülle das array 
			key[i] = this.key.charAt(i % this.key.length());       // mit dem key auf (wiederhole key wenn nötig)
		}                                                          // danch
		char[] text = ciphertext.toCharArray();                    // wandle input in ein char[] um
		for (int i = 0; i < text.length; i++) {                    // anschließend gehe den text durch
			int numInAlphaText = text[i] - erstesZeichen;          // ermittle die nummer des buchstaben in unserem alphabet (' ' bis '~')
			int numInAlphaKey  =  key[i] - erstesZeichen;          // und die nummer für den key
			int posNeusZeichen = numInAlphaText;                   // dann gehen wir vom Input-Zeichen aus
			posNeusZeichen -= numInAlphaKey;                       // und gehen den key "zurück"
			posNeusZeichen += offset;                              // und "verschiebe" das neue Zeichen um den offset, damti 'a' das erste Zeichen ist
			while(posNeusZeichen < 0)                              // wenn die Zahl jetzt aber unter 0 läuft
				posNeusZeichen += anzahlBuchstaben;                // addiere Anzahl der Buchstaben hinzu (muss so gemacht werden, da % nicht < 0 funtkioniert)
			posNeusZeichen = posNeusZeichen % anzahlBuchstaben;    // und stelle sicher dass der Wert innerhalb des Alphabetes bleibt
			posNeusZeichen += erstesZeichen;                       // danach erzeuge druckbares Zeichen indem wieder vom ersten Zeichen ausgegangen wird
			text[i] = (char) posNeusZeichen;                       // zuletzt wird die Zahl in einen Buchstaben verwandelt und erstetzt das Eingabe-Zeichen
		}                                                          // nachdem alle Zeichen ersetzt wurden
		return new String(text);                                   // gib neuen String aus den ersetzten Zeichen zurück
	}                                                              // und feiere eine funktionieren Crypto
	public String getStrengthRating() {                            //#um die Stärke der Verschlüsselung vergleichen zu können
		if(key.length() >=  inputLength)                           //#vergleiche die Schlüssellänge mit vorher gemerkter Länge der Eingabe
			return "major governments";                            //#wenn Key länger ist gibt gute Bewertung zurück
		else		                                               //#ansonsten
			return "big sister";                                   //#gibt eine schlechte Bewertung zurück
	}                                                              //#als Bewertung
	/******************** END LATIN ********************************/
	
	public static void main(String[] args) {
		System.out.println("bla");
		String longVigenereKey =
				"koancadlocevAcsEbOcOceejbennIrropduxUlArjEykEebrOgdarovOshej";
		VigLatinErlebnis vigenereLatin = new VigLatinErlebnis(longVigenereKey);
		System.out.println(vigenereLatin.encrypt(
		"OTPs are secure but the key exchange is tricky"));
		System.out.println("y#p!crh~sey(Ed(XuVgYg}i\"dln{Ovz\"$u}z_%");
		System.out.println(vigenereLatin.decrypt("y#p!crh~sey(Ed(XuVgYg}i\"dln{Ovz\"$u}z_%"));
	}


}
