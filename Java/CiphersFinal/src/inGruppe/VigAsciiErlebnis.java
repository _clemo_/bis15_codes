package inGruppe;
import at.ac.fhstp.prog1.Cipher;

public class VigAsciiErlebnis extends VigLatinErlebnis{  
	// erlebniserzählung zu Vigenere als ASCII
	/************* Konstruktor für ASCII ***********************/
	public VigAsciiErlebnis(String key) {                      // da wir von Vigenere erben
		super(key);                                            // müssen wir den geerbten Konstruktor aufrufen
		super.offset = 0;                                      // das offset verändern
	}                                                          // und feiern, dass die Klasse keine Arbeit macht
	/***********************************************************/
	
	@Override
	public String prepareText(String input){
		return input;
	}
	
	public static void main(String[] args) {
		String shortVigenereKey = "v1genere";
		Cipher vigenereAscii = new VigAsciiErlebnis(shortVigenereKey);
		System.out.println(vigenereAscii.decrypt(
		"Nyae]Ugel%MePeeKX}gIXV[Ki1WXnGrRf OKae^KpP"));
	}
}
