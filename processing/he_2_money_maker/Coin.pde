class Coin {
  public String path_to_img = "opengraph.png";
  private PImage _img = null;
  private int x;
  private int y;
  private int speed;
  private int diameter;

  public Coin(int x, int y, int diameter, int speed) {
    this.x = x;
    this.y = y;
    this.diameter = diameter;
    this.speed = speed;

    if (path_to_img != null)
      _img = loadImage(path_to_img);
  }

  void move(){
    y += speed;
  }

  public void Paint() {
    if (_img != null)
      image(_img, x, y, diameter, diameter);
    else {
      fill(200, 150, 10);
      ellipseMode(CORNER);
      ellipse(x, y, diameter, diameter);
    }
  }

  private boolean isBetween(int start, int val, int end) {
    return start <= val && val <= end;
  }

  public boolean overlaps(int x, int y, int w, int h) {
    return 
      (isBetween(x, this.x, x+w) || isBetween(x,this.x+this.diameter,x+w)) // überlappung auf x-achse
      && 
      (isBetween(y, this.y, y+h) || isBetween(y,this.y+this.diameter,y+h)); // überlappung auf y-achse
  }
  
  public boolean isConsumed(Bucket b){
    int[] row = b.GetOpeningRow();
    return overlaps(row[0], row[1], row[2], row[3]);
  }
  
  public boolean throughTheFloor(){
    return y > height;
  }
}