class Bucket{
  private int x;
  private int y;
  private int bucketHeight;
  private int bucketWidth;
  private String path = "bitbucket.png";
  private PImage _img = null;
  private int collectedCoins;
  
  public Bucket(int w, int h, int x, int y){
    collectedCoins = 0;
    this.bucketHeight = h;
    this.bucketWidth = w;
    set_center(x,y);
    
    if(path != null)
      _img = loadImage(path);
  }
  
  public Bucket(int w, int h){
    this(w,h,0,0);
  }
  
  public Bucket(){
    this(210/2, 243/2);
  }
  
  public void set_center(int x, int y){
    int hw = (bucketWidth+1)/2;
    
    this.x = x - hw;
    this.y = y;
  }
  
  public void Paint(){
    if(_img != null)
      image(_img,x,y,bucketWidth, bucketHeight);
    else{
      fill(0,0,255);
      rect(x,y,bucketWidth, bucketHeight);
    }
  }
  
  public int[] GetOpeningRow(){
    return new int[]{x,y,this.bucketWidth, 5};
  }
  
  public void addCoin(){
    collectedCoins += 1;
  }
  
  public int getCoins(){
    return collectedCoins;
  }
}