class Snowflake{
  PImage snowflake;
  int x,y,speed,diameter;
  
  Snowflake(int x, int y, int diameter, int speed, PImage snowflake){
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.snowflake = snowflake;
    this.diameter = diameter;
  }
  
  void paint(){
    imageMode(CORNER);
    image(snowflake,x,y,diameter,diameter);
  }
  
  void animate(){
    y += speed;
  }
  
  boolean isVisible(){
    return y < height;
  }
  
  public void resetY(int y){
    this.y = y;
  }
}