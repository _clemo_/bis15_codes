Bucket b = null;
int coin_amount = 100;
ArrayList<Coin> all_the_coins = new ArrayList<Coin>();

Snowflake[] snow;

boolean collidesWithOtherCoin(Coin c) {
  for (Coin o : all_the_coins) {
    if (c.overlaps(o.x, o.y, o.diameter, o.diameter))
      return true;
  }
  return false;
}

void setup() {
  size(500, 500);
  b = new Bucket();

  for (int i = 0; i < coin_amount; ++i) {
    Coin c;
    do {
      c = new Coin(int(random(50, width-50)), int(random(-2000, -50)), 50, int(random(2, 6)));
    } while (collidesWithOtherCoin(c));
    all_the_coins.add(c);
  }

  snow = new Snowflake[100];
  PImage flake = loadImage("snowflake.png");
  for (int i = 0; i < 100; ++i)
    snow[i] = new Snowflake(int(random(50, width-50)), int(random(-2000, -50)), 30, int(random(1, 3)), flake);
}

void draw() {
  background(99);

  if (b.getCoins() >= coin_amount/2)
    for (Snowflake f : snow) {
      f.paint();
      f.animate();
      if (!f.isVisible())
        f.resetY( int(random(-2000, -50)));
    }

  if (all_the_coins.size() > 0) {
    b.Paint();

    for (int i = 0; i < all_the_coins.size(); ++i) {
      Coin c = all_the_coins.get(i);
      c.Paint();
      c.move();

      if (c.isConsumed(b)) {
        b.addCoin();
        all_the_coins.remove(c);
        --i;
      } else if (c.throughTheFloor()) {
        all_the_coins.remove(c);
        --i;
      }
    }
  } else {
    fill(0);
    text("GAME OVER", width/2, height/2);
    text(b.getCoins()+"/"+coin_amount, width/2, height/2+20);
  }
}

void mouseMoved() {
  b.set_center(mouseX, mouseY);
}