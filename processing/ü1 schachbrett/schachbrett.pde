int tile_size = 50; //<>//
boolean use_colors=false;
boolean draw_rects=true;
boolean re_draw = true;

void setup(){
  size(400,400);
  ellipseMode(CORNER);
}

int rnd(){
  return int(random(0, 255));
}

void mousePressed(){
  re_draw = true;
  if(mouseButton == RIGHT){
    draw_rects = !draw_rects;
  }else{
    use_colors = !use_colors;
  }
}

void draw(){
  if(!re_draw) 
    return;
  background(255);
  int total_size = 400;
  for(int x = 0; x < total_size/tile_size;++x)
    for(int y = 0; y < total_size/tile_size;++y){
      boolean fill = (x%2 == 0 && y%2 == 1) || (x%2 == 1 && y%2 == 0);
      int r=255,g=255,b=255;
      if(fill){
        if(use_colors){
          r=rnd();
          g=rnd();
          b=rnd();
        }else{
          r=0;
          g=0;
          b=0;
        }
      }
      fill(r,g,b);
      
      if(draw_rects)
        rect(x*tile_size,y*tile_size,tile_size,tile_size);
      else
        ellipse(x*tile_size,y*tile_size,tile_size,tile_size);
    }
  re_draw = !re_draw;
}