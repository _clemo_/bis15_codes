class Calculator
  def numeric?(s)
    Float(s) != nil rescue false
  end

  def evaluate(string, print=true)
    ret = nil
    ret = 0 if string.length == 0
    ret = string.to_i if numeric? string
    
    if not(ret.nil?)
      return ret
    end
  
    ["+","-","*","/"].each do |symbol|
        symbol.strip!
        idx = string.rindex symbol
        next if idx.nil?

        l = string[0...idx].strip
        r = string[idx+1..-1].strip

        tmp = 0
        if symbol == "*" 
          tmp = evaluate(l,false)*evaluate(r,false)
        elsif symbol == "+"
          tmp = evaluate(l,false)+evaluate(r,false) 
        elsif symbol == "-"
          tmp = evaluate(l,false)-evaluate(r,false)
        else
          tmp = evaluate(l,false).fdiv(evaluate(r,false))
        end
        
        return tmp
    end
  end
end
