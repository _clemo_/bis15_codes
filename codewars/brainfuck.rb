def get_jump_markers(code)
	markers_stack = []
	markers_mapped = {}
	pos = 0
	while pos < code.length
		case code[pos]
			when '['
				markers_stack << pos
				markers_mapped[pos]  = nil
			when ']'
				top_of_stack = markers_stack.last
				markers_mapped[top_of_stack] = pos
				markers_stack.delete top_of_stack
		end
		pos += 1
	end

	markers_mapped 
end

def get_geverse_marker markers, marker
	markers.select{|k,v| v == marker}.keys.first
end

def brain_luck(code, input)
  output = ''
  
  pointer = 0
  vals = []
  i = 0

  markers = get_jump_markers code

  while i < code.length
  	c = code[i]
  	puts "code: #{c}"
  	case c
	  	when ','
	  		puts "\tread:#{input[0]} = #{input[0].ord}"
	  		vals[pointer] = input[0].ord
	  		input = input[1..-1]
	  		i+=1
	  	when '.'
	  		puts "\twrite: #{vals[pointer]} = #{vals[pointer].chr}"
	  		output += vals[pointer].chr
	  		puts "\t\tnew output: #{output}"
	  		i+=1
	  	when '>'
	  		puts "\tnext idx (old idx = #{pointer})"
	  		pointer += 1
	  		vals << 0 if vals.length <= pointer
	  		i+=1
	  	when '<'
	  		puts "\tprev idx (old idx = #{pointer})"
	  		pointer-=1
	  		i+=1
	  	when '+'
	  		print "\t #{vals[pointer]}+=1"
	  		vals[pointer]+=1
	  		i+=1
	  		vals[pointer] = 0 if vals[pointer] == 256 # fake 8 bit
	  		puts " = #{vals[pointer]}"
	  	when "-"
	  		print "\t #{vals[pointer]}-=1"
	  		vals[pointer]-=1
	  		i+=1
	  		vals[pointer] = 255 if vals[pointer] == -1 # fake 8 bit
	  		puts " = #{vals[pointer]}"
	  	when "["
	  		puts "loop begin (val = #{vals[pointer]})"
	  		if vals[pointer] == 0
	  			i = markers[i]+1 
	  		else
	  			i+=1
	  		end
	  	when "]"
	  		puts "loop end"
	  		i = get_geverse_marker markers, i
	  	else
	  		puts "ERROR: #{c}"
	  		i+=1
  	end
  end

  output
end

puts brain_luck(',>,<[>[->+>+<<]>>[-<<+>>]<<<-]>>.', 8.chr + 9.chr)